﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using Topshelf;

namespace Automate.Portal.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var module = Process.GetCurrentProcess().MainModule;
            var contentRoot = Path.GetDirectoryName(module.FileName);

            if ((Debugger.IsAttached || args.Contains("run")))
            {
                contentRoot = Directory.GetCurrentDirectory();
            }

            HostFactory.New(service =>
            {
                service.Service(() => new WindowsServiceHost(contentRoot));
                service.RunAsLocalSystem();
                service.SetDescription("Portal de governança de processos automatizados");
                service.SetServiceName("AutoMate PGPA Light");                
            }).Run();
        }
    }
}