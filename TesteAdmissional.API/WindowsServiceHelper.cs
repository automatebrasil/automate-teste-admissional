﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace Automate.Portal.API
{
    public class WindowsServiceHelper
    {
        private readonly string serviceName;

        public WindowsServiceHelper(string serviceName)
        {
            this.serviceName = serviceName;
        }

        public ServiceControllerStatus GetStatus()
        {
            var sc = new ServiceController(serviceName);
            return sc.Status;
        }

        public void Start()
        {
            var proc = new Process();
            var info = new ProcessStartInfo("cmd.exe", $"/c net start {serviceName}");
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.UseShellExecute = true;
            info.Verb = "runas";
            proc.StartInfo = info;
            proc.Start();
            proc.WaitForExit();
        }
    }
}