﻿using Automate.Portal.Domain.Service;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Threading;
using Topshelf;

namespace Automate.Portal.API
{
    internal class WindowsServiceHost : ServiceControl
    {
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly string contentPath = null;
        private ILogger logger = null;
        private IWebHost webHost = null;

        public WindowsServiceHost(string contentPath)
        {
            this.contentPath = contentPath;
        }

        public bool Start(HostControl hostControl)
        {
            try
            {
                webHost = WebHost.CreateDefaultBuilder()
                    .UseKestrel(options =>
                    {
                        options.Listen(IPAddress.Parse("0.0.0.0"), 9000);
                        options.AddServerHeader = false; // Server data is removed from any response header
                    })
                    .UseContentRoot(contentPath)
                    .UseStartup<Startup>()
                    .PreferHostingUrls(true) // removes the warning "Binding to endpoints defined in UseKestrel() instead.Overriding address(es) URL. Binding to endpoints defined in UseKestrel() instead."
                    .Build();

                webHost.RunAsync(cancellationTokenSource.Token);

                logger = webHost.Services.GetRequiredService<ILogger<WindowsServiceHost>>();
                logger.LogInformation("Serviço iniciado com sucesso");

                return true;
            }
            catch (Exception ex)
            {
                Log(ex);

                return false;
            }
        }

        public bool Stop(HostControl hostControl)
        {
            cancellationTokenSource.Cancel();

            logger.LogInformation("Serviço finalizado com sucesso");

            return true;
        }

        private void Log(Exception ex)
        {
            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "Logs", "LogService.txt");
                var message = $"{DateTime.Now} Exception: {ex.Message} - InnerException: {ex.InnerException?.Message} - StackTrace: {ex.StackTrace}";

                File.AppendAllLines(path, new string[] { message });
            }
            catch (Exception)
            {
                // Não lançar exceção caso houver erro na gravação do arquivo
            }
        }
    }
}
