﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Automate.Portal.API
{
    public class ExceptionHandlerFilter : IExceptionFilter
    {
        private static ILogger<ExceptionHandlerFilter> logger = null;


        public void OnException(ExceptionContext context)
        {
            var message = string.Empty;
            var response = context.HttpContext.Response;

            if (logger == null)
            {
                logger = context.HttpContext.RequestServices.GetService(typeof(ILogger<ExceptionHandlerFilter>)) as ILogger<ExceptionHandlerFilter>;
            }

            if (context.Exception is UnauthorizedAccessException)
            {
                message = "Acesso negado";
                response.StatusCode = (int)HttpStatusCode.Forbidden;
            }
            else
            {
                message = context.Exception.Message;
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            logger.LogError(context.Exception, "ST: " + context.Exception.StackTrace);

            context.ExceptionHandled = true;
            response.ContentType = "application/json";

            Task.WaitAll(response.WriteAsync($"{{\"message\": \"{message}\",\"type\":\"{context.Exception.GetType()}\"}}"));
        }
    }
}
