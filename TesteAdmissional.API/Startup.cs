﻿using Automate.Portal.AutomateBPA;
using Automate.Portal.Domain;
using Automate.Portal.Domain.Message;
using Automate.Portal.Domain.Service;
using Automate.Portal.Domain.Service.Repository;
using Automate.Portal.Shared;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Targets;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automate.Portal.API
{
    public class Startup
    {
        public static readonly SymmetricSecurityKey Key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("@@automate.local"));

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;        
        }

        //     var builder = new ConfigurationBuilder()
        //.SetBasePath(env.ContentRootPath)
        //.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        //.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
        //     builder.AddEnvironmentVariables();
        //     Configuration = builder.Build();

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var settingsManager = new AppSettingsManager();

            //Configuration.GetSection("Configuration").Bind(automateEnterprise1_0);

            var AutomateEnterprise = settingsManager.Read();

            //var pgpaConn = Configuration.GetConnectionString("PGPA");
            //var backupConn = Configuration.GetConnectionString("PGPA_BACKUP");

            services.RegisterServices(AutomateEnterprise, settingsManager);

            //new Automate.Portal.Expurgo.Monitor(AutomateEnterprise.ConnectionStrings.PGPA, AutomateEnterprise.ConnectionStrings.PGPA_BACKUP, 24).Start();

            // Configurar autenticação
            ConfigureAuthentication(services);

            // Registrar HttpContext para ser utilizado na SessionService
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Inicializar MVC
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                             .RequireAuthenticatedUser()
                             .Build();

                config.Filters.Add(new AuthorizeFilter(policy));
                config.Filters.Add(new ExceptionHandlerFilter());
            })
            .AddXmlSerializerFormatters()
            .AddXmlDataContractSerializerFormatters();

            services.AddResponseCompression();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            ConfigureLog(loggerFactory);

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "DENY");
                await next();
            });

            app.UseAuthentication();

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseDefaultFiles();
            app.UseResponseCompression();
            app.UseStaticFiles();
            app.UseStatusCodePages();

            app.UseMvc();
        }

        //Task.Factory.StartNew(() =>
        //{
        //    while (true)
        //    {
        //        Thread.Sleep(TimeSpan.FromHours(12));

        //        var auditoria = false;
        //        var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
        //        using (var scope = scopeFactory.CreateScope())
        //        {
        //            scope.ServiceProvider.GetRequiredService<ExpurgoRepository>();
        //            scope.ServiceProvider.GetService<ExpurgoService>().ExpurgarAuditoria();
        //        }
        //    }
        //});

        public void ConfigureAuthentication(IServiceCollection services)
        {
            var openIdConfiguration = new OpenIdConnectConfiguration();
            openIdConfiguration.SigningKeys.Add(Key);
            openIdConfiguration.Issuer = "automate.local";

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.Audience = "automate.local";
                options.Configuration = openIdConfiguration;
            });
        }

        public void ConfigureLog(ILoggerFactory loggerFactory)
        {
            var configuration = new LoggingConfiguration();

            configuration.AddRuleForAllLevels(new FileTarget
            {
                Layout = "${longdate} ${level} ${logger} - ${message} ${exception:maxInnerExceptionLevel=3}",
                FileName = "Logs/Log.txt",
                ArchiveAboveSize = 5242880,
                MaxArchiveFiles = 1,
                ConcurrentWrites = true,
                Encoding = Encoding.UTF8
            });

            loggerFactory.AddNLog();
            NLog.LogManager.Configuration = configuration;
        }
    }
}
