﻿using System;

namespace TesteAdmissional.Domain.Model
{
    public enum Sexo
    {
        Masculino,
        Feminino
    }

    public class PessoaFiltro
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public int? Sexo { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public int? IdadeDe { get; set; }
        public int? IdadeAte { get; set; }
    }

    public class Pessoa
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public Sexo Sexo { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public int Idade => Age(Nascimento);
        public DateTime Nascimento { get; set; }

        int Age(DateTime birthDate)
        {
            var laterDate = DateTime.Today;

            int age;
            age = laterDate.Year - birthDate.Year;

            if (age > 0)
            {
                age -= Convert.ToInt32(laterDate.Date < birthDate.Date.AddYears(age));
            }
            else
            {
                age = 0;
            }

            return age;
        }

        public Pessoa()
        {
        }
    }
}
