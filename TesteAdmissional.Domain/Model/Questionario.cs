﻿using System.Collections.Generic;

namespace TesteAdmissional.Domain.Model
{
    public class Questionario
    {
        public string TipoDesenvolvedor { get; set; }
        public string TipoDesenvolvedorDescricao { get; set; }
        public List<KeyValuePair<string, int>> Tecnologias { get; set; }

        public Questionario()
        {
            Tecnologias = new List<KeyValuePair<string, int>>();
        }
    }
}
