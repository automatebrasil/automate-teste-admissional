﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TesteAdmissional.Domain.Model
{
    public class Candidato
    {
        [Key]
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Questionario { get; set; }
        public DateTime Cadastro { get; set; }
        public DateTime? Inicio { get; set; }
        public DateTime? Fim { get; set; }
        public double Duracao => (Inicio.HasValue && Fim.HasValue) ? (Fim.Value - Inicio.Value).TotalMinutes : 0;
    }

    public class QuestionarioItem
    {
        public Questionario Obter(string questionario)
        {
            if (string.IsNullOrEmpty(questionario))
            {
                var Questionario = new Questionario
                {
                    Tecnologias = new List<KeyValuePair<string, int>>(){
                        Build("SQL"),
                        Build("C#"),
                        Build("MVC .NET"),
                        Build("EntityFramework"),
                        Build("LINQ"),
                        Build("AngularJS"),
                        Build("Angular"),
                        Build("Regex"),
                        Build("Bootstrap"),
                        Build("CSS"),
                        Build("Sass"),
                    }
                };
                return Questionario;
            } else
                return Newtonsoft.Json.JsonConvert.DeserializeObject<Questionario>(questionario);
        }

        KeyValuePair<string, int> Build(string x) => new KeyValuePair<string, int>(x, 0);

        public string GetQuestionario(Questionario x)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(x);
        }
    }
}