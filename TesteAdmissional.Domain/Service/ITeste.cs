﻿namespace TesteAdmissional.Domain.Service
{
    public interface ITeste
    {
        string Teste();
        TestBody Info();
    }
}
