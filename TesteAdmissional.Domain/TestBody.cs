﻿namespace TesteAdmissional.Domain
{
    public class TestBody
    {
        public string Titulo { get; set; }
        public string Objetivo { get; set; }
        public string Entrada { get; set; }
        public string Resultado { get; set; }
    }
}
