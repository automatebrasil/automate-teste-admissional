﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TesteAdmissional.Controllers.Core
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [AllowAnonymous]
    public class MyController : ControllerBase
    {
    }
}
