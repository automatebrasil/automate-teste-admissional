﻿using TesteAdmissional.Domain;
using TesteAdmissional.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;
using TesteAdmissional.Controllers.Core;
using TesteAdmissional.Domain.Service;
using TesteAdmissional.Domain.Model;

namespace TesteAdmissional.Controllers
{
    public class ProdutoController : MyController
    {
        public readonly ProdutoService servico;

        public ProdutoController(ProdutoService servico)
        {
            this.servico = servico;
        }

        [HttpGet("listar")]
        public IActionResult Listar()
        {
            var resultado = servico.Listar();

            return Ok(resultado);
        }

        [HttpGet]
        public IActionResult Obter(int id)
        {
            var resultado = servico.Obter(id);

            return Ok(resultado);
        }

        [HttpDelete]
        public IActionResult Remover(int id)
        {
            servico.Remover(id);

            return Ok();
        }

        [HttpPut]
        public IActionResult Salvar([FromBody]Produto g)
        {
            servico.Salvar(g);

            return Ok();
        }
    }
}