﻿using TesteAdmissional.Domain;
using TesteAdmissional.Service;
using TesteAdmissional.Service.Repository;
using TesteAdmissional.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Targets;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TesteAdmissional
{
    public class Startup
    {
        public static readonly SymmetricSecurityKey Key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("@@automate.local"));

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Configuration.GetConnectionString("Database");

            services.AddDbContext<AutomatePortalContext>(options =>
              options.UseSqlServer("Data Source=LAPTOP-HMHVGO14\\AMENTERPRISE11;Initial Catalog=TesteAdmissional;User=teste;Password=auto@123")
            );

            //// Registrar HttpContext para ser utilizado na SessionService
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<BaseRepository>();
            services.AddTransient<ProdutoService>();

            // Inicializar MVC
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                             .RequireAuthenticatedUser()
                             .Build();

                config.Filters.Add(new AuthorizeFilter(policy));
                config.Filters.Add(new ExceptionHandlerFilter());
            })
            .AddXmlSerializerFormatters()
            .AddXmlDataContractSerializerFormatters();

            services.AddResponseCompression();
            services.AddCors();
        }

        public void Configure(AutomatePortalContext automatePortal, IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            ConfigureLog(loggerFactory);

            automatePortal.Database.EnsureCreated();
            // automatePortal.Database.Migrate();

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "DENY");
                await next();
            });

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseDefaultFiles();
            app.UseResponseCompression();
            app.UseStaticFiles();
            app.UseStatusCodePages();

            app.UseMvc();
        }

        public void ConfigureLog(ILoggerFactory loggerFactory)
        {
            var configuration = new LoggingConfiguration();

            configuration.AddRuleForAllLevels(new FileTarget
            {
                Layout = "${longdate} ${level} ${logger} - ${message} ${exception:maxInnerExceptionLevel=3}",
                FileName = "Logs/Log.txt",
                ArchiveAboveSize = 5242880,
                MaxArchiveFiles = 1,
                ConcurrentWrites = true,
                Encoding = Encoding.UTF8
            });

            loggerFactory.AddNLog();
            NLog.LogManager.Configuration = configuration;
        }
    }
}
