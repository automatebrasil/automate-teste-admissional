﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using Topshelf;

namespace TesteAdmissional
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var module = Process.GetCurrentProcess().MainModule;
            var contentRoot = Path.GetDirectoryName(module.FileName);

            if ((Debugger.IsAttached || args.Contains("run")))
            {
                contentRoot = Directory.GetCurrentDirectory();
            }

            HostFactory.New(service =>
            {
                service.Service(() => new WindowsServiceHost(contentRoot));
                service.RunAsLocalSystem();
                service.SetDescription("Teste de Admissão");
                service.SetServiceName("AutoMate");                
            }).Run();
        }
    }
}