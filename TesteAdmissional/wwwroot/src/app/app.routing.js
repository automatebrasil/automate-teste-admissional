'use strict';

(function () {
    angular.module(appName).config(Router)

    function Router($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/')

        $stateProvider
            
            // Aplication
            .state('app', {
                abstract: true,
                controller: 'appController as vm',
                templateUrl: 'app/layout/content.html'
            })
            // produto
            .state('app.home', {
                url: '/',
                controller: 'produtoController as vm',
                templateUrl: 'app/components/produto/produto.html',
            })
            // produto
            .state('app.produto-item', {
                url: '/produto/{id}',
                controller: 'produtoItemController as vm',
                templateUrl: 'app/components/produto/produto-item.html',
            })            
    }

})()
