'use strict';

angular.module(appName).controller('produtoController', ProdutoController)

function ProdutoController(toaster, $anchorScroll, produtoService) {
    var vm = this
    vm.itemSelecionado = null
    vm.item = null

    function inicializar() {
        $anchorScroll()
        carregar()
        vm.show = true
    }

    vm.icon = 'fa fa-minus-square pull-right'
    vm.hidden = true
    vm.collapse = collapse
    function collapse() {
        if (vm.hidden)
            vm.icon = 'fa fa-plus-square pull-right'
        else
            vm.icon = 'fa fa-minus-square pull-right'
        vm.hidden = !vm.hidden
    }

    vm.carregar = carregar
    function carregar() {

            produtoService
                .listar()
                .then(function (data) {
                    vm.items = data
                })
    }

    vm.remover = remover
    function remover(item) {
        swal({
            title: 'Confirma a exclusão?',
            text: 'Grupo: ' + item.nome,
            icon: 'warning',
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
        }, function (confirm) {
            if (confirm) {                
                produtoService.remover(item.id).then(function()
                {
                    carregar()
                })
            }
        });
    }

    inicializar()
}