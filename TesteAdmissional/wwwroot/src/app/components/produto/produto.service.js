'use strict';

angular.module(appName).service('produtoService', produtoService)

function produtoService($http, apiEndpoint) {
    var endpoint = apiEndpoint + 'Produto'

    return {
        obter: function (id) {
            return $http({
                url: endpoint,
                method: 'get',
                params: { id: id || '' }
            }).then(function (response) {
                return response.data
            })
        },
        listar: function () {
            return $http({
                url: endpoint + "/listar",
                method: 'get',
            }).then(function (response) {
                return response.data
            })
        },
        salvar: function (item) {
            return $http({
                url: endpoint,
                method: 'put',
                data: item
            }).then(function (response) {
                return response.data
            })
        },
        remover: function (id) {
            return $http({
                url: endpoint,
                method: 'delete',
                params: { Id: id || '' }
            }).then(function (response) {
                return response.data
            })
        },
    }
}