'use strict';

angular.module(appName).controller('produtoItemController', ProdutoItemController)

function ProdutoItemController($stateParams, $state, toaster, produtoService) {
    var vm = this

    function inicializar() {

        vm.id = $stateParams.id

        vm.novo = vm.id == 0

        if (!vm.novo) {
            produtoService.obter(vm.id)
                .then(function (data) {
                    vm.item = data
                })
        }
    }

    vm.salvar = salvar
    function salvar() {
        produtoService
            .salvar(vm.item)
            .then(function () {
                toaster.pop('success', '', vm.novo ? 'Item adicionado com sucesso.' : 'Item atualizado com sucesso')
                $state.go('app.produto')
            })
    }

    inicializar()
}
