// Download any type of file example

function Download(arquivo) {
    return $http({
        url: apiEndpoint + 'fila/arquivo/download/' + arquivo,
        responseType: "arraybuffer",
        method: 'get'
    })
        .then(function (response) {
            var data = new Blob([response.data])

            FileSaver.saveAs(data, arquivo)
        })
}