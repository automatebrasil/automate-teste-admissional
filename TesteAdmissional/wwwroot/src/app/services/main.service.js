'use strict';

angular.module(appName).service('mainService', MainService)

function MainService($http, $localStorage, apiEndpoint) {
    
    return {

        usuario: function()
        {
            return $localStorage.testeAdmissionalUser;
        },
        
        iniciarTeste: function(user)
        {
            $localStorage.testeAdmissionalUser = user            
        },

        finalizarTeste: function()
        {
            localStorage.removeItem('testeAdmissionalUser');
        },

        listar: function (tipo) {
            return $http({
                url: apiEndpoint + 'dominio',
                method: 'get',
                params: { campo: tipo }
            }).then(function (response) {
                return response.data
            })
        },
        listarPerfis: function () {
            return $http({
                url: apiEndpoint + 'dominio/perfil',
                method: 'get'
            }).then(function (response) {
                return response.data
            })
        }
    }

}
