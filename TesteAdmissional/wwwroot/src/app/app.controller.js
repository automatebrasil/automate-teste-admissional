'use strict';

angular.module(appName).controller('appController', AppController)

function AppController($state) {
    var vm = this
    vm.showVersion = true
    vm.title ='Automate Brasil - Teste Admissional'

    angular.element('body').removeClass('gray-bg')

    vm.toggle = toggle
    function toggle() {
        vm.showVersion = !vm.showVersion
    }

    vm.menuClick = menuClick
    function menuClick(url) {
        if (url) $state.go(url, { id: 1 })
    }

    function inicializar() {

        vm.menu = [            
            { nome: 'Candidato', menuLink: 'app.candidato', menuCss: 'fa fa-user', subMenu: null },
        ]
    }

    inicializar()
}