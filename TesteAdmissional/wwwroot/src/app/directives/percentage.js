/**
* 
*/
angular.module('filters', [])
	.filter('percentage', function () {
		return function ($filter, input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
		};
	});
