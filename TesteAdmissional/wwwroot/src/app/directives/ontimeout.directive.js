'use strict';

angular.module(appName).directive('ontimeout', TimeoutDirective)

function TimeoutDirective() {
    return {
        restrict: 'E',
        templateUrl: 'app/directives/ontimeout.html',      
    }
}

