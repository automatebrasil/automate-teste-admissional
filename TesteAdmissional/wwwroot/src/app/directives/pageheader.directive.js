'use strict';

angular.module(appName).directive("pageheader", PageHeaderDirective)

function PageHeaderDirective() {
    return {
        restrict: "E",
        template: '<div class="wrapper row border-bottom white-bg page-heading"><div class="col-lg-10"><h2>{{ text }}</h2></div><div class="col-lg-2"></div></div>',
        scope: {
            text: "@"
        }
    }
}
