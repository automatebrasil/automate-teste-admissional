'use strict';

angular.module(appName).directive('paginacao', PaginacaoDirective)

function PaginacaoDirective() {
    return {
        restrict: 'E',
        templateUrl: 'app/directives/paginacao.html',
        scope: {
            // options: '=',
            total: '=',
            index: '@',
            limite: '@',
            click: '&',
            onLimitChanged: '&',
        },

        controller: function ($scope, paginacaoService) {

            $scope.c = paginacaoService.getItem(parseInt($scope.index))

            // $scope.$watch('options', function (data) {                
            // }, true);

            $scope.restart = function () {
                $scope.c.p = 0
                $scope.c.zzz = 0
            }

            $scope.goLastPage = function () {
                if ($scope.total / $scope.c.tp == 1) {
                    $scope.paginaClick({ index: $scope.total })
                } else {
                    $scope.c.zzz = parseInt($scope.total / $scope.c.tp)
                    $scope.paginaClick({ index: $scope.total })
                }
            }

            $scope.goFirstPage = function () {
                $scope.c.zzz = 0
                $scope.paginaClick({ index: 1 })
            }

            $scope.firstDotsClick = function () {
                $scope.c.zzz -= 1
                if ($scope.c.zzz < 0) {
                    $scope.c.zzz = 0
                }
            }

            $scope.lastDotsClick = function () {
                $scope.c.zzz += 1
            }

            $scope.paginaClick = function (data) {
                $scope.c.p = data.index // this variable is used only to hightlight current page
                $scope.click(data)
            }

            $scope.limiteAlterado = function (limite) {
                $scope.onLimitChanged(limite)
            }
        }
    }
}