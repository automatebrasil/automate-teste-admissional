'use strict';

angular.module(appName).directive('sideNavigation', SideNavigationDirective)

function SideNavigationDirective($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            // Call metsi to build when user signup
            scope.$watch('authentication.user', function () {
                $timeout(function () {
                    element.metisMenu()
                })
            })

        }
    }
}
