'use strict';

angular.module(appName).directive('ibox', IboxDirective)

function IboxDirective() {
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: 'app/directives/ibox.html',
        scope: {
            enablefullscreen: '@',
            enablecollapse: '@',
            hidetitle: '@',
            pageIndex: '=',
            menu: '@',
            text: '@',
            loaded: '@',
            count: '@',
            limite: '@',
            pageCount: '=',
            showPage: '@',
            blocked: '@',
            refresh: '&',
            pageClick: '&',
            menuClick: '&',
            limitChanged: '&'
        },

        controller: function ($scope, $element, $timeout) {

            $scope.isColapsed = false

            if ($scope.menu)
                $scope.menuItems = JSON.parse($scope.menu)

            $scope.refreshClick = function () {
                $scope.refresh()
            }

            $scope.itemMenuClick = function (data) {
                $scope.menuClick(data)
            }

            $scope.pagLimiteAlterado = function (data) {
                $scope.limitChanged(data)
            }

            $scope.paginaClick = function (data) {
                if (data.index > 0) // fix bug dot click
                    $scope.pageClick({ index: data.index })
            }

            $scope.showhide = function () {

                var ibox = $element.find('div.ibox')
                var icon = $element.find('i#chevron')

                var content = ibox.find('div.ibox-content')

                $scope.isColapsed = !$scope.isColapsed

                content.slideToggle(400)
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down')
                ibox.toggleClass('').toggleClass('border-bottom')

                $timeout(function () {
                    ibox.resize()
                    ibox.find('[id^=map-]').resize()
                }, 50)
            }

            $scope.fullscreen = function () {
                var ibox = $element.find('div.ibox')
                var button = $element.find('i.fa-expand')

                angular.element('body').toggleClass('fullscreen-ibox-mode')
                button.toggleClass('fa-expand').toggleClass('fa-compress')
                ibox.toggleClass('fullscreen')

                $timeout(function () {
                    angular.element(window).trigger('resize')
                }, 100)
            }
        }
    }
}
