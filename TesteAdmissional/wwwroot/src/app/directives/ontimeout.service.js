'use strict';


angular.module(appName).service('onTimeoutService', OnTimeoutService)

function OnTimeoutService($interval) {

    var totalTime = null
    var timer = null
    var carregarData = null

    var viewBag = {
        countdown: null,
        ultimaAtualizacao: null
    }

    var decreamentCountdown = function () {
        viewBag.countdown -= 1
        if (viewBag.countdown < 1) {
            viewBag.countdown = totalTime
            carregarData()
        }
    }

    return {
        cancel: function () {
            $interval.cancel(timer);
        },

        start: function (scope, total, func) {
            totalTime = total
            carregarData = func
            $interval.cancel(timer)
            viewBag.countdown = totalTime
            viewBag.ultimaAtualizacao = Date.now()
            timer = $interval(decreamentCountdown, 1000, viewBag.countdown)
            scope.cds = viewBag
        }
    }
}



