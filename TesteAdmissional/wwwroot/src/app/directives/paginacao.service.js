'use strict';

angular.module(appName).service('paginacaoService', PaginacaoService)

// This service is responsible to keep a shared memory between diferent pagination so they work as one
function PaginacaoService() {

    var items = [
        { p: 0, zzz: 0, tp: 10 }, // 0 Auditoria       
        { p: 0, zzz: 0, tp: 10 }, // 1 Fila        
        { p: 0, zzz: 0, tp: 10 }, // 2 Execucao       
        { p: 0, zzz: 0, tp: 10 }, // 3 Log Fila
        { p: 0, zzz: 0, tp: 10 }, // 4 Log Execução
        { p: 0, zzz: 0, tp: 10 }, // 5 Auditoria Automate
    ]

    return {
        
        getItem: function (index) {
            return items[index];
        }
    }
}
