'use strict';

angular.module(appName).directive('yesnocircle', YesNoCircleDirective)

function YesNoCircleDirective() {
    return {
        restrict: 'E',
        templateUrl: 'app/directives/yesnocircle.html',
        scope: {
            ativo: '@'                        
        }        
    }
}