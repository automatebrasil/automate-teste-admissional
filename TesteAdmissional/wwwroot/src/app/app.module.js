'use strict';

var appName = 'automate'
var hasToLog = true

angular.module(appName, [
    'angular.chosen',
    'datePicker',
    'angularPromiseButtons',
    'chart.js',
    'ngAnimate',
    'ngCookies',
    'ngFileSaver',
    'ngMask',
    'ngMessages',
    'ngSanitize',
    'ngStorage',
    'textAngular',
    'toaster',
    'ui.bootstrap',
    'ui.footable',
    'ui.router',
    'ui.sortable',
    'ui.toggle',
    'jsonFormatter',
    'angular-json-tree',
    'ngMessages',
    'ngclipboard',
    'monospaced.qrcode',
    'filters',
])
    .config(Configure)
    .run(Run)


/**
* Configurações gerais da aplicação
*/
function Configure($httpProvider) {

    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {}
    }

    $httpProvider.defaults.headers.get = { 'X-Frame-Options': 'DENY' }
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT'
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache'
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache'

    swal.setDefaults({
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#2A6EBB',
        confirmButtonText: 'OK'
    })

    Chart.defaults.global.legend.display = true
    Chart.defaults.scale.ticks.beginAtZero = true
    Chart.defaults.global.colors = ['#5E897C', '#5CAE8A', '#F4B46B', '#F8D863', '#F4766B', '#758189', '#C9E5F7', '#D47782', '#9FBFCF', '#E1EEF9']

    moment.locale('pt-br')
}

/**
* Configuração do controle de permissões nas rotas da aplicação
*/
function Run($transitions) {
}

