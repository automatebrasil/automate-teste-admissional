﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace TesteAdmissional.Service
{
    public static class MapperExtesions
    {
        public static IList<TResult> ToMappedList<TSource, TResult>(this IEnumerable<TSource> source)
        {
            return source.Select(i => Mapper.Map<TResult>(i)).ToList();
        }

        public static TResult[] ToMappedArray<TSource, TResult>(this IEnumerable<TSource> source)
        {
            return source.Select(i => Mapper.Map<TResult>(i)).ToArray();
        }

        public static T ToMapped<T>(this object item)
        {
            if (item == null)
            {
                return default(T);
            }

            return Mapper.Map<T>(item);
        }
    }
}
