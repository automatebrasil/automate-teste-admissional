﻿using TesteAdmissional.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace TesteAdmissional.Service.Repository
{
    public class AutomatePortalContext : DbContext
    {
        public AutomatePortalContext(DbContextOptions<AutomatePortalContext> options) : base(options) { }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Candidato> Candidatos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Entity<Candidato>().Property(b => b.Nome).HasMaxLength(100);
            modelBuilder.Entity<Candidato>().Property(b => b.Email).HasMaxLength(11);
            modelBuilder.Entity<Produto>().Property(b => b.Nome).HasMaxLength(100);
            modelBuilder.Entity<Produto>().Property(b => b.Preco).HasColumnType("decimal(8,2)");
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
