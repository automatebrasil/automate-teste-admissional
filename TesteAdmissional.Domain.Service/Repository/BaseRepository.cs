﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace TesteAdmissional.Service.Repository
{
    public class BaseRepository : IDisposable
    {
        public readonly AutomatePortalContext context;

        public BaseRepository(AutomatePortalContext context)
        {
            this.context = context;            
        }

        public virtual IQueryable<T> ListaTodos<T>() where T : class
        {
            return context.Set<T>().AsNoTracking();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public virtual IQueryable<T> Listar<T>(Expression<Func<T, bool>> where) where T : class
        {
            return context.Set<T>().AsNoTracking().Where(where);
        }

        public virtual T Obter<T>(Expression<Func<T, bool>> where) where T : class
        {
            return context.Set<T>().AsNoTracking().FirstOrDefault(where);
        }

        public virtual T Atualiza<T>(T entity) where T : class
        {
            context.Set<T>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;

            context.SaveChanges();

            return entity;
        }

        public virtual T Adiciona<T>(T entity) where T : class
        {
            context.Set<T>().Add(entity);
            context.SaveChanges();
            return entity;
        }

        public virtual bool Remove<T>(T entity) where T : class
        {
            context.Set<T>().Remove(entity);
            context.SaveChanges();
            return true;
        }

        public DbSet<T> Set<T>() where T : class
        {
            return context.Set<T>();
        }
    }
}
