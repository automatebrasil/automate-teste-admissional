﻿using System;
using System.Collections.Generic;
using TesteAdmissional.Domain.Model;
using TesteAdmissional.Service.Repository;

namespace TesteAdmissional.Service
{
    public class ProdutoService
    {
        private readonly BaseRepository repo;

        public ProdutoService(BaseRepository repo)
        {
            this.repo = repo;
        }

        public List<Produto> Listar()
        {
            return new List<Produto>(); // Falta implementar
        }

        public void Salvar(Produto g)
        {
            throw new NotImplementedException();
        }

        public void Remover(int id)
        {
            throw new NotImplementedException();
        }

        public Produto Obter(int id)
        {
            throw new NotImplementedException();
        }
    }
}
